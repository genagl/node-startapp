/*
    CREATING NEW PE-RESOURCE
    run: npm run create:resource ***new-resource-name*** 
    ***new-resource-name***  -- module name/resource name(single world by CamelCase)
*/ 
const fs = require('fs');
const {
    Input,
    AutoComplete
} = require('enquirer');

const createResource = (module, resourceName) => {
    const moduleDir = `${ __dirname }/../src/modules/${ module }`;
    const plural = (resourceName.slice(-1) === "y" 
        ? 
        `${resourceName.slice(0, resourceName.length - 1)}ies` 
        : 
        `${resourceName}s` ).toLowerCase();

    // create entity
    const moduleEntityDir = `${ __dirname }/../src/modules/${ module }/entities`;
    const resourceFileName = `${ resourceToLowerCase(resourceName) }.entity.ts`;
    if(!fs.existsSync( moduleEntityDir )) {
        fs.mkdirSync( moduleEntityDir );
    }
    fs.writeFileSync( 
        `${ moduleEntityDir }/${ resourceFileName }`, 
`import { Entity } from "typeorm"; 
import { ObjectType } from '@nestjs/graphql';
import { PELandedEntity } from "@common/entities";

@Entity("${plural}")
@ObjectType("${resourceName}", { description: '${plural}' })
export class ${resourceName}Entity extends PELandedEntity {
    
}`);

    // create create-dto
    const moduleDtoDir = `${ __dirname }/../src/modules/${ module }/dto`;
    const resourceDtoFileName = `create-${resourceToLowerCase(resourceName)}.dto.ts`;
    if(!fs.existsSync( moduleDtoDir )) {
        fs.mkdirSync( moduleDtoDir );
    }
    fs.writeFileSync( 
        `${ moduleDtoDir }/${ resourceDtoFileName }`, 
`import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";

@InputType("${resourceName}Input")
export class Create${resourceName}Dto extends CreatePEDto {
    
}`);

    // create update-dto
    const resourceUpdateDtoFileName = `update-${resourceToLowerCase(resourceName)}.dto.ts`; 
    fs.writeFileSync( 
        `${ moduleDtoDir }/${ resourceUpdateDtoFileName }`, 
`import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { Create${resourceName}Dto } from './create-${resourceToLowerCase(resourceName)}.dto';

@InputType("${resourceName}Input")
export class Update${resourceName}Dto extends PartialType(Create${resourceName}Dto) {

}`);

    // create service
    const resourceServiceFileName = `${resourceToLowerCase(resourceName)}.service.ts`;  
    fs.writeFileSync( 
        `${ moduleDir }/${ resourceServiceFileName }`, 
`import { Injectable } from "@nestjs/common";  
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { ${resourceName}Entity } from "./entities/${resourceToLowerCase(resourceName)}.entity";
import { Create${resourceName}Dto } from "./dto/create-${resourceToLowerCase(resourceName)}.dto";

@Injectable()
export class ${resourceName}Service extends PERepository<${resourceName}Entity, Create${resourceName}Dto> {
    constructor( 
        @InjectRepository(${resourceName}Entity)
        private readonly _${resourceToLowerCase(resourceName, "_")}Repository: Repository<${resourceName}Entity>,
    ) {
        super(_${resourceToLowerCase(resourceName, "_")}Repository, "${resourceToLowerCase(resourceName)}"); 
    }
}`);  

    // create controller
    const resourceControllerFileName = `${resourceToLowerCase(resourceName)}.controller.ts`; 
    fs.writeFileSync( 
        `${ moduleDir }/${ resourceControllerFileName }`, 
`import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { PEController } from "@common/pe.controller";
import { ${resourceName}Service } from "./${resourceToLowerCase(resourceName)}.service";

@Controller('${plural}') 
@ApiTags("${plural}") 
export class ${resourceName}Controller extends PEController {
    constructor( private readonly _${resourceToLowerCase(resourceName, "_")}Service: ${resourceName}Service ){
        super( _${resourceToLowerCase(resourceName, "_")}Service );
    }        
}`);  


    // create resolver
    const resourceProviderFileName = `${resourceToLowerCase(resourceName)}.resolver.ts`;  
    fs.writeFileSync( 
        `${ moduleDir }/${ resourceProviderFileName }`, 
`import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { Create${resourceName}Dto } from "./dto/create-${resourceToLowerCase(resourceName)}.dto";
import { ${resourceName}Entity } from "./entities/${resourceToLowerCase(resourceName)}.entity";
import { ${resourceName}Service } from "./${resourceToLowerCase(resourceName)}.service";


@Resolver(() => ${resourceName}Entity)
export class ${resourceName}Resolver extends PEResolver(
    ${resourceName}Entity,
    Create${resourceName}Dto, 
    "${resourceName}"
) {
    constructor( 
        private readonly _service: ${resourceName}Service,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
    ) {
        super( _service, _lands, _users ); 
    }
}`);  


    // edit module file
    updateModuleFile(module, resourceName);

    // finish
    console.log(`Resource ${resourceName} created in ${module} module.`);
}

const askModule = () => {
    // Create a prompt for the module
    let modules = [];
    const filenames = fs.readdirSync(`${__dirname}/../src/modules` );  
    filenames.forEach(file => { 
        if(fs.lstatSync(`${__dirname}/../src/modules/${file}`).isDirectory()) { 
            modules.push(file);  
        } 
    }); 
    const _askModule = new AutoComplete({
        name: 'module',
        message: 'Choose parent module', 
        choices: modules
    });
    return _askModule;
}

const ask = () => {
    const askName = new Input({
        name: 'name',
        message: 'What is Resource name?'
    }); 
    
    // Run the prompts and display the user's choices
    const run = async () => {
        const name = await askName.run();
        const resourceName = name.charAt(0).toUpperCase() + name.slice(1);
        const module = await askModule().run();
        createResource(module, resourceName);
    }
    run();
}

const bootstrap = () => {
    const args =  process.argv;
    if(args[2]) {
        const names = args[2].split("/");
        if(names.length === 1) {
            resourceName = names[0].charAt(0).toUpperCase() + names[0].slice(1);
            const run = async () => {
                const module = await askModule().run();
                createResource(module, resourceName);
            }
            run();
            return;
        }
        else if (names.length === 2) {
            resourceName = names[1].charAt(0).toUpperCase() + names[1].slice(1);
            moduleName = names[0];
            createResource(moduleName, resourceName);
            return;
        } 
        else {
            ask();
            return;
        }
    }
    else {
        ask();
        return;
    }
}

const updateModuleFile = (module, resourceName) => { 
    const moduleFileUri = `${ __dirname }/../src/modules/${module}/${module}.module.ts`;
    fs.readFile( 
        moduleFileUri, 
        {encoding: 'utf-8'}, 
        function(err, data) { 
            if(err) {
                throw err;
            }
            const content = data; 
            processFile( content );
        } 
    );
    // add to *.module.ts
    const processFile = ( content ) => {
        const resourceEntity = `${resourceName}Entity`;
        const resourceController = `${resourceName}Controller`; 
        const resourceService = `${resourceName}Service`; 
        const resourceResolver = `${resourceName}Resolver`; 
        
        // add "imports", "controllers", "providers" to module if not exists
        let mod = content.match(/@Module\(([^)]+)\)/)?.[1]; 
        
        if(mod === "{}") {
            content = content.replace(
                mod, 
                `{ 
    imports: [], 
    providers: [], 
    controllers: [] 
}`
            );
            content = `import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@users/users.module';
import { LandsModule } from '@lands/lands.module'; 

${content}`;
        }
        
        // get import TypeOrmModule and add entity
        let tom = content.match(/(?<=imports\s*).*?(?=\s*])/gi);
        if( tom[0].indexOf("TypeOrmModule.forFeature")  === -1 ) {
            // typeorm import not exists
            content = content.replace(tom[0], `${tom[0]}TypeOrmModule.forFeature([ ${resourceEntity} ]),
        LandsModule,
        UsersModule`)
        }
        else {
            // typeorm import exists
            let entities =  content.match(/(?<=TypeOrmModule.forFeature\s*).*?(?=\s*])/gi); 
            content = content.replace( entities[0], entities[0] + `, ${resourceEntity}`);
        }

        // add controller
        let controllers = content.replace(/ {2,}/g, " ").match(/(?<=controllers:\s*).*?(?=\s*])/gi); 
        controllers = controllers[0].replace("[", "").trim();
        content = content.replace(controllers, `${resourceController}, ${controllers}`);

        // add provider
        let providers = content.match(/(?<=providers:\s*).*?(?=\s*])/gi); 
        providers = providers[0].replace("[", "").trim(); 
        content = content.replace(providers, `${resourceService}, ${resourceResolver}, ${providers}`);
        content = `import { ${resourceName}Service } from './${resourceToLowerCase(resourceName)}.service';
import { ${resourceName}Resolver } from './${resourceToLowerCase(resourceName)}.resolver';
import { ${resourceName}Controller } from './${resourceToLowerCase(resourceName)}.controller';
import { ${resourceName}Entity } from './entities/${resourceToLowerCase(resourceName)}.entity';
${content}`;

        // save module file
        fs.writeFileSync( moduleFileUri, content );
    }
}
let resourceSnakeName;
const resourceToLowerCase = (resourceName, dlm="-") => {
    if(resourceSnakeName) {
        return resourceSnakeName
    }
    else {
        resourceSnakeName       = resourceName.replace(/[A-Z]/g, letter => `${dlm}${letter.toLowerCase()}`);
        if( resourceSnakeName.indexOf("-p-e-") === 0 ) {
            resourceSnakeName   = resourceSnakeName.replace("-p-e-", "pe-");
        }
        if( resourceSnakeName.indexOf("-") === 0 ) {
            resourceSnakeName   = resourceSnakeName.substring(1);
        }
    }
    return resourceSnakeName;
    //return resourceName.toLowerCase();
}

bootstrap();