import { setURLImage } from "@common/utils/add-url-to-image.util";
import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { GqlContextType, GqlExecutionContext } from "@nestjs/graphql";
import { Request } from 'express';
import { map, Observable } from "rxjs";

@Injectable()
export class UpdateURLIconInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        let req: Request;
        if (context.getType<GqlContextType>() === 'graphql') {
            const gqlContext: GqlExecutionContext = GqlExecutionContext.create(context);
            req = gqlContext.getContext().req;
        }
        else {
            const httpContext = context.switchToHttp();
            req = httpContext.getRequest();
        }
        return next 
            .handle()
            .pipe(map((value: any) => recursivelySendIcons(value, req) ))
    }
}

const recursivelySendIcons = (value: any, request: Request) => {
    if (Array.isArray(value)) {
        return value.map(v => recursivelySendIcons(v, request));
    }
    let v = value;
    if (!!v?.logotype_name) { 
        if(v.logotype_name.indexOf("http") == -1) {
            v = {
                ...v,
                icon:       setURLImage(request, v.logotype_name),
                logotype:   setURLImage(request, v.logotype_name),
            };
        }
        else {
            v = {
                ...v,
                icon:       v.logotype_name,
                logotype:   v.logotype_name,
            }; 
        }  
    } 
    if(!!v?.thumbnail_name) {
        if(v.thumbnail_name.indexOf("http") == -1) {
            v = {
                ...v,
                thumbnail:          setURLImage(request, v.thumbnail_name),
                defaultThumbnail:   setURLImage(request, v.thumbnail_name),
            };
        }
        else {
            v = {
                ...v,
                thumbnail:          v.thumbnail_name,
                defaultThumbnail:   v.thumbnail_name,
            }; 
        } 
    }
    return v; 
}
 