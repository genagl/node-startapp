import { Role } from "@common/interface";
import { IPEBase } from "@common/interface/pe-interface";
import { LAND_TYPES } from "@lands/land-types/interface";
import { UserEntity } from "@users/entities";

export interface ILand extends IPEBase {    
    logotype?: string;
    logotype_name?: string;
    vk_client_id?: string;
    thumbnail?: string;
    thumbnail_name?: string;
    thumbnail_id?: number;
    name: string;
    description: string;
    email?: string | null;
    email2?: string | null;
    address?: string | null;
    help_url?: string | null;
    vk_app_id?: string | null;
    vk?: string | null;
    vk2?: string | null;
    telegramm?: string | null;
    telegramm2?: string | null;
    phone?: string | null;
    phone2?: string | null;
    youtube?: string | null;
    defaultThumbnail_name?: string | null;
    defaultThumbnail_id?: string | null;
    defaultThumbnail?: string | null;
    icon?: string | null;
    domain?: string | null;
    domain_content?: string | null;
    domain_description?: string | null;
    startDate?: Date | false;
    finishDate?: Date | false;
    images?: string[];
    images_names?: string[];
    event_types?: string[] | [];
    land_type?: [LAND_TYPES]
    land_data?: any;
    domain_type?: string | false;
    status?: string | false;
    isPattern?: boolean | false;
    isRegisterUser?: boolean | false;
    enabledReglament?: boolean | false;
    reglamentTitle?: string
    reglament?: string;
    owner_user?: UserEntity | false;
    geo: string[];
    user_verify_account?: boolean | false;
    availableRoles: Role[];
    clientUrl: string;
    link1: string;
    link2: string; 
}