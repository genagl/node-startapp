import { Resolver } from '@nestjs/graphql';  
import { LandTypeEntity } from './entities/land-type.entity';
import { LandTypesService } from './land-types.service';

@Resolver(() => LandTypeEntity)
export class LandTypesResolver {
  constructor(private readonly landTypesService: LandTypesService) {}
 
}
