import { registerEnumType } from "@nestjs/graphql";

export enum LAND_TYPES  {
    Place       = "Place",
    Event       = "Event",
    Team        = "Team",
    Treasures   = "Treasures",
}
registerEnumType(LAND_TYPES, {name: 'LandType'})