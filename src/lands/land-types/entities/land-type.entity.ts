import { ObjectType, Field, ID  } from '@nestjs/graphql'; 
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity("land-types")
@ObjectType("LandType")
export class LandTypeEntity {
 
  @PrimaryGeneratedColumn()
  @Field(() => ID, { nullable: true,  description: "Uniq identifier." })
  id: number; 

  @Column("varchar", {nullable: false })
  @Field(() => String, { description: 'Name' })
  name: string;

  @Column("varchar", {nullable: false })
  @Field(() => String, { description: 'Color' })
  color: string;

  @Column("varchar", {nullable: false })
  @Field(() => String, { description: 'Icon' })
  icon: string;

}
