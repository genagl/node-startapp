import { CreateLandTypeInput } from './create-land-type.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateLandTypeInput extends PartialType(CreateLandTypeInput) {
  @Field(() => Int)
  id: number;
}
