import { Controller, Get } from "@nestjs/common";
import { ApiOperation, ApiTags } from "@nestjs/swagger";
import { PEController } from "src/common/pe.controller";
import { LandService } from "./land.service";

@Controller('lands') 
@ApiTags("lands") 
export class LandController extends PEController {
    constructor( private readonly _landService: LandService ){
        super( _landService );
    }    
    
    
}