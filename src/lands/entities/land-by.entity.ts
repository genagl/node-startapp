 
import { POST_STATUS } from "@common/interface";
import { ILand } from "@lands/interface";
import { LAND_TYPES } from "@lands/land-types/interface";
import { Field, HideField, ID, Int, ObjectType } from "@nestjs/graphql";
import { ApiProperty } from "@nestjs/swagger";
import { UserLandEntity } from "@users/entities";
import { UserEntity } from "@users/entities/user.entity";
import { Role } from "@users/models/roles.enum";
import { IsEmail, IsPhoneNumber, MaxLength, MinLength } from "class-validator";
import { GraphQLJSONObject } from "graphql-type-json";
import { OneToMany } from "typeorm";

@ObjectType() 
export class LandByEntity implements ILand  { 
    
    @MaxLength(150)
    @Field(() => String, { description: "Title of pe-resource", nullable: true }) 
    @ApiProperty({ nullable: true  })
    title?: string; 
     
    @MaxLength(2500)
    @Field(() => String, { 
        defaultValue: "", 
        description: "HTML content of pe-resource", 
        deprecationReason: "Outdated. Used for compatibility with the API wp-server. Use the 'content' field",
        nullable: true 
    }) 
    @ApiProperty({ example: " ", nullable: true  })
    post_content?: string;
 
    @MaxLength(2500)
    @Field(() => String, { defaultValue: "", description: "HTML content of pe-resource", nullable: true }) 
    @ApiProperty({ example: " ", nullable: true  })
    content?: string;
 
    @MinLength(3)
    @MaxLength(50)
    @Field(() => String, {
        nullable: true, 
        defaultValue: "", 
        description: "Resource optional password. Max length — 50." 
    }) 
    @ApiProperty({ 
        example: "", 
        required: false, 
        nullable: true, 
        description: "Resource optional password. Max length — 50." 
     })
    password: string;
 
    @ApiProperty({ example: "", required: false, description: 'Order' })
    @Field(() => Int, { nullable: true } )
    order?: number;
 
    @ApiProperty({ example: "", required: false, description: 'Author ID' })
    @Field(() => Int, { nullable: true } )
    author_id?: number;
    
    @Field(() => UserEntity, { description: 'Resource author' }) 
    author?: UserEntity;  
    
    @Field(() => UserEntity, { 
        description: 'Resource author',
        deprecationReason: "Use 'author' field! This field added for backward compatibility with legacy APIs. Will be removed in the next version."
     }) 
    post_author?: UserEntity;  
     
    @Field(() => POST_STATUS, { 
        nullable: true, 
        description: 'Resource visible status', 
        defaultValue: POST_STATUS.PUBLISH 
    })
    @ApiProperty({ 
        enum: POST_STATUS, 
        enumName: "POST_STATUS", 
        example: POST_STATUS.PUBLISH, 
        required: false, 
        description: 'Resource visible status' 
    })
    post_status: POST_STATUS;
 
    @ApiProperty({ nullable: true  })
    @Field({  description: 'Is resource blocked by admin?', defaultValue: false }) 
    is_blocked?: boolean;
 
    @ApiProperty({ nullable: true  })
    @Field(() => String, {  nullable: true, defaultValue: "", description: "URL of logotype" })  
    logotype?: string;
 
    @ApiProperty({ nullable: true  })
    @Field(() => String, {  nullable: true, defaultValue: "", description: "Name with ext of logotype file" })  
    logotype_name?: string;
 
    @ApiProperty({ nullable: true  })
    @Field(() => String, {  nullable: true, defaultValue: "", description: "VK client ID" })  
    vk_client_id?: string;
 
    @ApiProperty({ nullable: true  })
    @Field(() => String, {  nullable: true, defaultValue: "", description: "URL of thumbnail" })  
    thumbnail?: string;
 
    @ApiProperty({ nullable: true  })
    @Field(() => String, {  nullable: true, defaultValue: "", description: "File name of media." }) 
    thumbnail_name?: string;
 
    @ApiProperty({ nullable: true  })
    @Field(() => ID, {  nullable: true, defaultValue: "", description: "Uniq ID of media file" })  
    thumbnail_id?: number;


    @MaxLength(150)
    @Field(() => String, {
        defaultValue: "New land", 
        description: "Name of land", 
        deprecationReason: "Use 'title' member",
        nullable: true 
    }) 
    @ApiProperty({ example: "New land", nullable: true  })
    name: string
     
    @MaxLength(450)
    @Field(() => String, {defaultValue: "", description: "Description of land", nullable: true }) 
    @ApiProperty({ example: "", nullable: true  })
    description: string
     
    @Field(() => String, {defaultValue: "", description: "Administrator email", nullable: true }) 
    @ApiProperty({ example: "", nullable: true  })
    @IsEmail()
    email?: string | null;
     
    @Field(() => String, {defaultValue: "", description: "Administrator email 2", nullable: true }) 
    @ApiProperty({ example: "", nullable: true  })
    @IsEmail()
    email2?: string | null;
      
    @Field(() => String, {defaultValue: "", description: "Public address", nullable: true }) 
    @ApiProperty({ example: "", nullable: true  })
    address?: string | null;
     
    @Field(() => String, { description: "Help site URL", nullable: true }) 
    @ApiProperty({ example: "", nullable: true  })
    help_url?: string | null;
      
    @Field(() => String, { description: "VK app ID", nullable: true }) 
    @ApiProperty({ nullable: true  })
    vk_app_id?: string | null;
      
    @Field(() => String, { description: "VK link", nullable: true }) 
    @ApiProperty({ nullable: true  })
    vk?: string | null;
     
    @Field(() => String, { description: "VK link 2", nullable: true }) 
    @ApiProperty({ nullable: true  })
    vk2?: string | null;
      
    @Field(() => String, { description: "telegram link ", nullable: true }) 
    @ApiProperty({ nullable: true  })
    telegramm?: string | null;
       
    @Field(() => String, { description: "telegram link 2", nullable: true }) 
    @ApiProperty({ nullable: true  })
    telegramm2?: string | null;
       
    @Field(() => String, { description: "Contact phone", nullable: true }) 
    @ApiProperty({ nullable: true  })
    @IsPhoneNumber()
    phone?: string | null;
      
    @Field(() => String, { description: "Contact phone 2", nullable: true }) 
    @ApiProperty({ nullable: true  })
    @IsPhoneNumber()
    phone2?: string | null;
       
    @Field(() => String, { description: "youtube link", nullable: true }) 
    @ApiProperty({ nullable: true  })
    youtube?: string | null; 
      
    @Field(() => String, { description: "Default image name", nullable: true }) 
    @ApiProperty({ nullable: true  })
    defaultThumbnail_name?: string | null;
    

    /* START ALIEN INJECTION */  
    @Field(() => String, { description: "Default image ID", nullable: true }) 
    @ApiProperty({ nullable: true  })
    defaultThumbnail_id?: string | null;
     
    @Field(() => String, { description: "Default image", nullable: true }) 
    @ApiProperty({ nullable: true  })
    defaultThumbnail?: string | null;
      
    @Field(() => String, { description: "Default icon", nullable: true }) 
    @ApiProperty({ nullable: true  })
    icon?: string | null;
      
    @Field(() => String, { description: "Domain", nullable: true }) 
    @ApiProperty({ nullable: true  })
    domain?: string | null;
     
    @Field(() => String, { description: "Domain content", nullable: true }) 
    @ApiProperty({ nullable: true  })
    domain_content?: string | null;
     
    @Field(() => String, { description: "Domain description", nullable: true }) 
    @ApiProperty({ nullable: true  })
    domain_description?: string | null;
         
    @Field(() => Date, { description: "Start date", nullable: true }) 
    @ApiProperty({ nullable: true  })
    startDate?: Date | false;
          
    @Field(() => Date,  { description: "Finish date", nullable: true }) 
    @ApiProperty({ nullable: true  })
    finishDate?: Date | false;
         
    @Field(() => [String], { defaultValue: [], description: "Images gallery", nullable: true }) 
    @ApiProperty({ nullable: true  })
    images?: string[];
         
    @Field(() => [String], { defaultValue: [], description: "Images gallery's names list. Use only for send names of new Images to server", nullable: true }) 
    @ApiProperty({ nullable: true  })
    images_names?: string[];
          
    @Field(() => [String], { defaultValue: [],  description: "Event types", nullable: true }) 
    @ApiProperty({ nullable: true  })
    event_types?: string[] | [];
         
    @Field(() => [LAND_TYPES], { description: "Land type (show getLandTypes)", nullable: "itemsAndList" }) 
    @ApiProperty({ nullable: false  })
    land_type?: [LAND_TYPES]
         
    @Field(() => GraphQLJSONObject, { description: "Free Land's data by LandTypes and LandTypeVariations", nullable: true }) 
    @ApiProperty({ type: GraphQLJSONObject, nullable: true  })
    land_data?: any;
         
    @Field(() => String, { description: "domain type", nullable: true }) 
    @ApiProperty({ nullable: true  })
    domain_type?: string | false;
         
    @Field(() => String,  { description: "Festival status", nullable: true }) 
    @ApiProperty({ nullable: true  })
    status?: string | false;
  
    @Field(() => Boolean, { description: "This Land is pattern for over new", nullable: true }) 
    @ApiProperty({ nullable: true  })
    isPattern?: boolean | false;
 
    @Field(() => Boolean, { description: "New user may register", nullable: true }) 
    @ApiProperty({ nullable: true  })
    isRegisterUser?: boolean | false;
 
    @Field(() => Boolean, { description: "Land reglament enabled", nullable: true }) 
    @ApiProperty({ nullable: true  })
    enabledReglament?: boolean | false;
 
    @MaxLength(150)
    @Field(() => String, {
        defaultValue: "Reglament", 
        description: "Reglament title",  
        nullable: true 
    })  
    reglamentTitle?: string
 
    @Field(() => String, {
        defaultValue: "Reglament content", 
        description: "Reglament content",  
        nullable: true 
    })  
    reglament?: string
   
    @Field(() => UserEntity, { description: "Land administrator", nullable: true })  
    owner_user?: UserEntity | false; 
          
    @Field(() => GraphQLJSONObject, { description: "Geo position", nullable: true }) 
    @ApiProperty({ nullable: false  })
    geo: string[];

    /* FINISH ALIEN INJECTION */
             
    @Field(() => Boolean, { description: "New User must e-mail activate account", nullable: true }) 
    @ApiProperty({ nullable: true  })
    user_verify_account?: boolean | false;
         
    @Field(() => [Role], { description: "List of available roles", nullable: true }) 
    @ApiProperty({ enum: Role, enumName: 'Role', type: [Role],  example: [Role.User], nullable: false  })
    availableRoles: Role[]
          
    @Field(() => String, { description: "URl of web-client", nullable: true }) 
    @ApiProperty({ nullable: false  })
    clientUrl: string;
         
    @Field(() => String, { description: "External link 1", nullable: true }) 
    @ApiProperty({ nullable: false  })
    link1: string;
         
    @Field(() => String, { description: "External link 2", nullable: true }) 
    @ApiProperty({ nullable: false  })
    link2: string; 
    /**/  

    /* https://orkhan.gitbook.io/typeorm/docs/many-to-many-relations */    
    @HideField()
    @OneToMany( () => UserLandEntity, userLand => userLand.user )
    public userLand: UserLandEntity[];
}