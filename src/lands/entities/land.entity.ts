 
import { PEEntity } from "@common/entities/pe.entity";
import { LAND_TYPES } from "@lands/land-types/interface";
import { Field, HideField, ID, ObjectType } from "@nestjs/graphql";
import { ApiProperty } from "@nestjs/swagger";
import { UserLandEntity } from "@users/entities";
import { UserEntity } from "@users/entities/user.entity";
import { Role } from "@users/models/roles.enum";
import { IsEmail, IsPhoneNumber, MaxLength } from "class-validator";
import { GraphQLJSONObject } from "graphql-type-json";
import { Column, Entity, OneToMany } from "typeorm";


@Entity("lands")
@ObjectType("Land", { description: 'lands' })
export class LandEntity extends PEEntity { 

    @Column("varchar", {length: 150, nullable: true, default: "New land" }) 
    @MaxLength(150)
    @Field(() => String, { 
        description: "Name of land", 
        deprecationReason: "Use 'title' member",
        nullable: true 
    }) 
    @ApiProperty({ example: "New land", nullable: true  })
    name: string;

    @Field(() => ID, {  
        deprecationReason: "No use! This field added for backward compatibility with legacy APIs. Will be removed in the next version.",
        nullable: true 
    }) 
    @ApiProperty({ nullable: true  })
    demiurg_id: string | number;
    
    @Column("varchar", {length: 2500, nullable: true, }) 
    @MaxLength(2500)
    @Field(() => String, {defaultValue: "", description: "Description of land", nullable: true }) 
    @ApiProperty({ example: "", nullable: true  })
    description: string;
    
    @Column("varchar", { nullable: true, default: "" })  
    @Field(() => String, {defaultValue: "", description: "Administrator email", nullable: true }) 
    @ApiProperty({ example: "", nullable: true  })
    @IsEmail()
    email?: string | null;
    
    @Column("varchar", { nullable: true, default: "" })  
    @Field(() => String, {defaultValue: "", description: "Administrator email 2", nullable: true }) 
    @ApiProperty({ example: "", nullable: true  })
    @IsEmail()
    email2?: string | null;
    
    @Column("varchar", { nullable: true, default: "" })  
    @Field(() => String, {defaultValue: "", description: "Public address", nullable: true }) 
    @ApiProperty({ example: "", nullable: true  })
    address?: string | null;
    
    @Column("varchar", { nullable: true, default: "" })  
    @Field(() => String, { description: "Help land URL", nullable: true }) 
    @ApiProperty({ example: "", nullable: true  })
    help_url?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "VK app ID", nullable: true }) 
    @ApiProperty({ nullable: true  })
    vk_app_id?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "VK link", nullable: true }) 
    @ApiProperty({ nullable: true  })
    vk?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "VK link 2", nullable: true }) 
    @ApiProperty({ nullable: true  })
    vk2?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "telegram link ", nullable: true }) 
    @ApiProperty({ nullable: true  })
    telegramm?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "telegram link 2", nullable: true }) 
    @ApiProperty({ nullable: true  })
    telegramm2?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Contact phone", nullable: true }) 
    @ApiProperty({ nullable: true  })
    @IsPhoneNumber()
    phone?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Contact phone 2", nullable: true }) 
    @ApiProperty({ nullable: true  })
    @IsPhoneNumber()
    phone2?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "youtube link", nullable: true }) 
    @ApiProperty({ nullable: true  })
    youtube?: string | null;  
 
    @Column("varchar", { nullable: true })  
    @ApiProperty({ nullable: true  })
    @Field(() => String, {  nullable: true, defaultValue: "", description: "URL of logotype" })  
    logotype?: string;
 
    @Column("varchar", { nullable: true })  
    @ApiProperty({ nullable: true  })
    @Field(() => String, {  nullable: true, defaultValue: "", description: "Name with ext of logotype file" })  
    logotype_name?: string;
  
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Default image name", nullable: true }) 
    @ApiProperty({ nullable: true  })
    defaultThumbnail_name?: string | null;

    /* START ALIEN INJECTION */
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Default image ID", nullable: true }) 
    @ApiProperty({ nullable: true  })
    defaultThumbnail_id?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Default image", nullable: true }) 
    @ApiProperty({ nullable: true  })
    defaultThumbnail?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Default icon", nullable: true }) 
    @ApiProperty({ nullable: true  })
    icon?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Domain", nullable: true }) 
    @ApiProperty({ nullable: true  })
    domain?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Domain content", nullable: true }) 
    @ApiProperty({ nullable: true  })
    domain_content?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Domain description", nullable: true }) 
    @ApiProperty({ nullable: true  })
    domain_description?: string | null;
        
    @Column("timestamp without time zone", { nullable: true })  
    @Field(() => Date, { description: "Start date", nullable: true }) 
    @ApiProperty({ nullable: true  })
    startDate?: Date | false;
        
    @Column("timestamp without time zone", { nullable: true })  
    @Field(() => Date,  { description: "Finish date", nullable: true }) 
    @ApiProperty({ nullable: true  })
    finishDate?: Date | false;
        
    @Column( "simple-array", { default:[], array: true, nullable: true })  
    @Field(() => [String], { defaultValue: [], description: "Images gallery", nullable: true }) 
    @ApiProperty({ nullable: true  })
    images?: string[];
        
    @Column( "simple-array", { default:[], array: true, nullable: true })  
    @Field(() => [String], { defaultValue: [], description: "Images gallery's names list. Use only for send names of new Images to server", nullable: true }) 
    @ApiProperty({ nullable: true  })
    images_names?: string[];
        
    @Column( "varchar", { default:[], array: true, nullable: true })  
    @Field(() => [String], { defaultValue: [],  description: "Event types", nullable: true }) 
    @ApiProperty({ nullable: true  })
    event_types?: string[] | [];
    
        
    @Column("text", { array: true, default: "{Administrator,User}"  }) 
    @Field(() => [Role], { description: "List of available roles", nullable: true }) 
    @ApiProperty({ enum: Role, enumName: 'Role', type: [Role],  example: [Role.User], nullable: false  })
    availableRoles: Role[];

        
    @Column( "simple-enum", { enumName: "LAND_TYPES", enum: LAND_TYPES, default: [LAND_TYPES.Place],  array: true, nullable: true }) 
    @Field(() => [LAND_TYPES], { description: "Land type (show getLandTypes)", nullable: "itemsAndList" }) 
    @ApiProperty({ nullable: false  })
    land_type?: LAND_TYPES[]; 
        
    @HideField( ) 
    land_id?: number | string;
        
    @Column("simple-json", { default: { data: "[Place]"}, nullable: true  }) 
    @Field(() => GraphQLJSONObject, { description: "Free Land's data by LandTypes and LandTypeVariations", nullable: true }) 
    @ApiProperty({ type: GraphQLJSONObject, nullable: true  })
    land_data?: any;
        
    @Column( "varchar", { nullable: true })  
    @Field(() => String, { description: "domain type", nullable: true }) 
    @ApiProperty({ nullable: true  })
    domain_type?: string | false;
        
    @Column( "varchar", { nullable: true })  
    @Field(() => String,  { description: "Festival status", nullable: true }) 
    @ApiProperty({ nullable: true  })
    status?: string | false;

    @Column("boolean", { nullable: true, default: false  })  
    @Field(() => Boolean, { description: "This Land is pattern for over new", nullable: true }) 
    @ApiProperty({ nullable: true  })
    isPattern?: boolean | false;

    @Column("boolean", { nullable: true, default: true })  
    @Field(() => Boolean, { description: "New user may register", nullable: true }) 
    @ApiProperty({ nullable: true  })
    isRegisterUser?: boolean | false;

    @Column("boolean", { nullable: true, default: true })  
    @Field(() => Boolean, { description: "Land reglament enabled", nullable: true }) 
    @ApiProperty({ nullable: true  })
    enabledReglament?: boolean | false;

    @Column("varchar", {nullable: true, default: "Reglament" })
    @MaxLength(150)
    @Field(() => String, { 
        description: "Reglament title",  
        nullable: true 
    })  
    reglamentTitle?: string

    @Column("text", {nullable: true, default: "Reglament content", })
    @Field(() => String, { 
        description: "Reglament content",  
        nullable: true 
    })  
    reglament?: string
   
    @Field(() => UserEntity, { description: "Land administrator", nullable: true })  
    owner_user?: UserEntity | false; 
        
    @Column("text", { array: true, nullable: true}) 
    @Field(() => GraphQLJSONObject, { description: "Geo position", nullable: true }) 
    @ApiProperty({ nullable: false  })
    geo: string[];

    /* FINISH ALIEN INJECTION */
            
    @Column("boolean", { nullable: true, default: true })  
    @Field(() => Boolean, { description: "New User must e-mail activate account", nullable: true }) 
    @ApiProperty({ nullable: true  })
    user_verify_account?: boolean | false;
         
    @Column("varchar", { nullable: true}) 
    @Field(() => String, { description: "URl of web-client", nullable: true }) 
    @ApiProperty({ nullable: false  })
    clientUrl: string;
        
    @Column("varchar", { nullable: true}) 
    @Field(() => String, { description: "External link 1", nullable: true }) 
    @ApiProperty({ nullable: false  })
    link1: string;
        
    @Column("varchar", { nullable: true}) 
    @Field(() => String, { description: "External link 2", nullable: true }) 
    @ApiProperty({ nullable: false  })
    link2: string; 
    /**/  

    /* https://orkhan.gitbook.io/typeorm/docs/many-to-many-relations */    
    @HideField()
    @OneToMany( () => UserLandEntity, userLand => userLand.user )
    public userLand: UserLandEntity[];
}