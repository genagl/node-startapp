import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LandEntity } from './entities/land.entity';
import { LandController } from './land.controller';
import { LandResolver } from './land.resolver';
import { LandService } from './land.service'; 
import { LandTypesModule } from './land-types/land-types.module';

@Module({
  controllers: [ LandController ],
  providers: [ LandService, LandResolver ],
  imports: [ TypeOrmModule.forFeature([ LandEntity ]), LandTypesModule ],
  exports: [ LandService ],
})
export class LandsModule {}