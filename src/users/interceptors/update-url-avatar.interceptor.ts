import { setURLAvatar } from "@common/utils";
import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { GqlContextType, GqlExecutionContext } from "@nestjs/graphql";
import { Request } from 'express';
import { map, Observable } from "rxjs";

@Injectable()
export class UpdateURLAvatarInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        let req: Request;
        if (context.getType<GqlContextType>() === 'graphql') {
            const gqlContext: GqlExecutionContext = GqlExecutionContext.create(context);
            req = gqlContext.getContext().req;
        }
        else {
            const httpContext = context.switchToHttp();
            req = httpContext.getRequest();
        }
        return next 
            .handle()
            .pipe(map((value: any) => recursivelySendAvatars(value, req) ))
    }
}

const recursivelySendAvatars = (value: any, request: Request) => {
    if (Array.isArray(value)) {
        return value.map(v => recursivelySendAvatars(v, request));
    }
    if (!!value?.avatar_name) { 
        const user = {
            ...value,
            avatar: setURLAvatar( request, value.avatar_name )
        } 
        return user;
    } 
    return value; 
}