import { InputType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Express } from 'express';
 
@InputType()
class FileUploadDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  file: Express.Multer.File;
}
 
export default FileUploadDto;