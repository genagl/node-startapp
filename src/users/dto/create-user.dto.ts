import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { Role } from '../models/roles.enum';
import { IsEmail, IsOptional, IsString, MinLength, Validate } from 'class-validator';
import { IsPasswordsMatchingConstraint } from '@common/decorators';
import { Field, InputType } from '@nestjs/graphql';

@InputType("UserInput")
export class CreateUserDto {

  @ApiProperty({default: "test@test.ru"})
  @Field(() => String )
  @IsEmail()
  email: string;

  @ApiProperty({default: "John Good", required: false })
  @Field(() => String, { nullable: true })
  @IsOptional() 
  @IsString()
  display_name?: string | null;

  @ApiProperty({default: "JohnGood", required: false })
  @Field({nullable: true})
  @IsOptional() 
  @IsString()
  user_login?: string | null;

  @ApiProperty({default: "", required: false })
  @Field({nullable: true})
  @IsOptional() 
  @IsString()
  phone?: string | null;

  @ApiHideProperty( ) 
  avatar?: string | null;

  @ApiHideProperty( ) 
  externalId?: string | null;

  @ApiProperty({default: "Good", required: false })
  @IsOptional() 
  @IsString()
  @Field({nullable: true})
  last_name?: string | null;

  @ApiProperty({default: "John", required: false })
  @IsOptional() 
  @IsString()
  @Field({nullable: true})
  first_name?: string | null;

  @ApiProperty({default: "", required: false })
  @Field({nullable: true})
  @IsOptional() 
  @IsString()
  user_descr?: string | null; 

  @ApiProperty({default: "111"})
  @IsString()
  @MinLength(3)
  @Field()
  password: string; 

  @ApiProperty({default: "111"})
  @IsString()
  @MinLength(3)
  @Validate(IsPasswordsMatchingConstraint)
  @Field()
  passwordRepeat: string;

  
  roles: Role[]
}
