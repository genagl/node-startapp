import { ApiProperty } from "@nestjs/swagger";
import { Field, HideField, ID, ObjectType } from '@nestjs/graphql';
import { FileEntity } from "src/files/entities/file.entity";
import { IsEmail, MaxLength, MinLength } from 'class-validator';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm"; 
import { Role } from "../models/roles.enum";
import { EXTERNAL_TYPE } from "@common/interface/external";
import { UserLandEntity } from "./user-land.entity"; 

@Entity("users")
@ObjectType("User", { description: "Basic ecosystem's user" })
export class UserEntity {
    @PrimaryGeneratedColumn()
    @Field(() => ID, { description: "Uniq user's identifier." })
    id: number;

    @Column() 
    @ApiProperty({ example: "test@test.com", nullable: false })
    @IsEmail()
    email?: string 
 
    @ApiProperty({ example: "test@test.com", nullable: false })
    @IsEmail()
    @Field(() => String, { deprecationReason: "Use field 'email'! This field added for backward compatibility with legacy APIs. Will be removed in the next version."  }) 
    user_email?: string 

    @Column("text", { array: true, default: "{User}"  }) 
    @Field(() => [Role], { description: "List of roles" }) 
    @ApiProperty({ enum: Role, enumName: 'Role', type: [Role],  example: [Role.User], nullable: false  })
    roles?: Role[]
 
    @Column( )
    @MinLength(3)
    @MaxLength(50)
    @HideField()
    @ApiProperty({ example: "111", description: "Min length — 3, max length — 50", })
    password: string

    @Column({length: 300, nullable: true})
    @MaxLength(300)
    @Field(() => String, { nullable: true, description: 'first name of the user' })  
    @ApiProperty({ example: "John", description: 'first name of the user' })
    user_descr?: string

    @Column({length: 20, nullable: true})
    @Field(() => String, { nullable: true, description: 'User login' })  
    @ApiProperty({ example: "John", description: 'User login' })
    user_login?: string

    @Column({length: 20, nullable: true})
    @Field(() => String, { description: 'User phone', defaultValue: "" })  
    @ApiProperty({ example: "", description: 'User phone' })
    phone?: string

    @Column({length: 90, nullable: true})
    @Field(() => String, { description: 'User avatar', defaultValue: ""})  
    @ApiProperty({ example: "", description: 'User avatar' })
    avatar?: string

    @Column({length: 90, nullable: true})
    @Field(() => String, { description: 'User avatar file name', defaultValue: ""})  
    @ApiProperty({ example: "", description: 'User avatar file name' })
    avatar_name?: string

    @Column({length: 20, nullable: true})
    @Field(() => String, { description: 'User avatar ID', defaultValue: ""})  
    @ApiProperty({ example: "", description: 'User avatar ID' })
    avatar_id?: string

    @Column({length: 90, nullable: true})
    @Field(() => String, { description: 'first name of the user', defaultValue: "John"})  
    @ApiProperty({ example: "John", description: 'first name of the user' })
    first_name?: string

    @Column({length: 90, nullable: true})
    @Field(() => String, { description: 'last name of the user', defaultValue: "Good" })  
    @ApiProperty({ example: "Good", description: 'last name of the user' })
    last_name?: string

    @Column({length: 180, nullable: true})
    @Field({ nullable: true, defaultValue: "John Good"})
    @ApiProperty({ example: "John Good", description: 'Esquire', nullable: true })
    display_name?: string
    
    @Column({length: 4, nullable: true})
    @Field(() => EXTERNAL_TYPE, { description: 'Social external', defaultValue: "PE"  })  
    @ApiProperty({ example: EXTERNAL_TYPE.PE, description: 'social external' })
    external?: EXTERNAL_TYPE

    @Column({ nullable: true })
    @Field({ nullable: true, description: 'is external user?', defaultValue: false })
    @ApiProperty({ example: false, description: 'is external user?' })
    is_external?: boolean
    
    @Column({ nullable: true })
    @Field(() => String, { description: 'social external ID', defaultValue: ""  })  
    @ApiProperty({ example: EXTERNAL_TYPE.PE, description: 'social external ID' })
    externalId ?: string

    @Column({ nullable: true })
    @Field({ nullable: true, description: 'User is blocked by admin?', defaultValue: false })
    @ApiProperty({ example: false, description: 'User is blocked by admin?' })
    is_blocked?: boolean
    
    @Field(() => [FileEntity], {nullable: true})
    @OneToMany(() => FileEntity, (file:FileEntity) => file.user )
    @ApiProperty({ type: FileEntity, isArray:true, example: ["image1.jpg", "image2.png"], nullable: true })
    files?: FileEntity[];

    /* https://orkhan.gitbook.io/typeorm/docs/many-to-many-relations */
    @OneToMany( () => UserLandEntity, userLand => userLand.land )
    @HideField()
    userLand?: UserLandEntity[];
    
}
/*
TODO: add createAt, updateAt and soft delete. https://stackru.com/questions/59059280/typeorm-softdelete-pochemu-voznikaet-oshibka-missingdeletedatecolumnerror
*/

