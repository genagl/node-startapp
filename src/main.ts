import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import * as cookieParser from "cookie-parser";
import * as express from 'express';
import { join } from 'path';
import { AppModule } from './app.module';
import { getSwagger } from './common/utils';

async function bootstrap() {
  const app = await NestFactory.create( AppModule );
  const configService = app.select(AppModule).get(ConfigService);
  const _static: string = configService.get("STATIC_PATH") || "";
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors({ credentials: true, origin: true });
  app.use('/uploads', express.static( join(__dirname, _static, 'uploads')) );
  app.use('/avatars', express.static( join(__dirname, _static, 'avatars')) );
  app.use('/static',  express.static( join(__dirname,  _static, 'static')) );
  app.use(cookieParser());
  app.enableCors({
    origin: '*',
    credentials: configService.get("NODE_ENV") !== "production",
  });

  if(!!configService.get("IS_SWAGGER")) {
    getSwagger( app );
  }
  await app.listen(configService.get("SERVER_PORT"), () => {
    console.log( `Start on post ${configService.get("SERVER_PORT")}` );
  });
  
}
bootstrap();

