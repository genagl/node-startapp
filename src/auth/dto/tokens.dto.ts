  
import { IsString } from "class-validator";
import { Directive, Field, Int, ObjectType } from "@nestjs/graphql";

@ObjectType("UserToken")
export class TokensEntity {
 
    @Field() 
    @IsString()
    access_token: string
    
    @Directive('@deprecated(reason: "This member must by empty and will be removed in the next version")')
    @Field({nullable: true, description: "This member must by empty and will be removed in the next version"}) 
    @IsString()
    refresh_token: string
    
    @Directive('@deprecated(reason: "This member must by empty and will be removed in the next version")')
    @Field({nullable: true, description: "This member must by empty and will be removed in the next version"}) 
    @IsString()
    token_type: string
 
    @Directive('@deprecated(reason: "Expire access token.")')
    @Field(() => Int, {nullable: true, description: "Expire access token."}) 
    expires_in: number
}