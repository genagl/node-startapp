  
import { IsString } from "class-validator";
import { Directive, Field, Int, ObjectType } from "@nestjs/graphql";

@ObjectType( )
export class TokenEntity {
 
    @Field() 
    @IsString()
    access_token: string
    
    @Directive('@deprecated(reason: "This member must by empty and will be removed in the next version")')
    @Field({nullable: true, description: "This member must by empty and will be removed in the next version"}) 
    @IsString()
    refresh_token: string
    
    @Directive('@deprecated(reason: "This member must by empty and will be removed in the next version")')
    @Field({nullable: true, description: "This member must by empty and will be removed in the next version"}) 
    @IsString()
    token_type: string
 
    @Directive('@deprecated(reason: "This member must by empty and will be removed in the next version")')
    @Field(() => Int, {nullable: true, description: "This member must by empty and will be removed in the next version"}) 
    expires_in: number
}