const bcrypt = require('bcrypt')

import { 
  ConflictException, 
  ForbiddenException, 
  Injectable, 
  Logger, 
  UnauthorizedException 
} from '@nestjs/common'
 
import { JwtService } from '@nestjs/jwt' 
import { add } from 'date-fns';  
import { UsersService } from '@users/users.service';
import { CreateUserDto } from '@users/dto/create-user.dto';
import { UserEntity } from '@users/entities/user.entity';
import { LoginDto } from './dto/login.dto'; 
import { ConfigService } from '@nestjs/config';
import { TokenService } from '@tokens/token.service'; 
import { CreateTokenDto } from '@tokens/dto/create-token.dto';
import { Tokens } from '@common/interface';
import { convertToSecondsUtil } from '@src/common/utils';

@Injectable()
export class AuthService {
  private logger:Logger = new Logger(AuthService.name);
  constructor(
    private readonly usersService: UsersService,
    private readonly tokenService: TokenService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService

  ) {}

  async refreshTokens (refreshToken: string, agent:string) : Promise<Tokens> {
    const token = this.tokenService.findOne(refreshToken);
    if(!token) {
      throw new UnauthorizedException();
    } 
    await this.tokenService.remove( (await token).id );
    if( new Date((await token).expire) < new Date() ) {
      throw new UnauthorizedException();
    }

    const userData = await this.usersService.findById( (await token)?.userId ).catch((err) => {
      this.logger.error(err);
      return null;
    })
    return this.generateTokens( userData, agent );
  }

  async validateUser(email: string, password: string): Promise<any> { 
    const user = await this.usersService.findByEmail(email);
    // console.log( email, user, password )  
    if (user && !bcrypt.compareSync(user.password, password) ) {
      const { ...result } = user;
      return result;
    } 
    return null;
  }

  async register(dto: CreateUserDto): Promise<UserEntity> {
    const user = await this.usersService.findByEmail(dto.email);
    if(user) {
      throw new ConflictException("This email now exists");
    }
    try {
      dto.password = bcrypt.hashSync( dto.password, bcrypt.genSaltSync(10))
      const userData = await this.usersService.create({
        ...dto,
        display_name: dto.display_name || `${dto.first_name} ${dto.last_name}`
      }).catch((err) => {
        this.logger.error(err);
        return null;
      });
      return userData;
    } catch (err) {
      this.logger.error(err);
      throw new ForbiddenException('Register error.');
    }
  }

  /* ONLY FOR REST -- TODO: fix need */
  async login(user: UserEntity) {
    return {
      token: this.jwtService.sign({ id: user.id }),
    };
  }

  //GG
  async signIn ( login: LoginDto, agent: string): Promise<Tokens> {
    const userData: any | UserEntity  = await this.usersService.findByEmail( login.login ).catch((err) => {
      this.logger.error(err);
      return null;
    });
    if ( !userData ) { 
      throw new UnauthorizedException("Login or password not correct!"); 
    }
    
    const userData2: UserEntity = await this.usersService.create( userData ); 
    // console.log( userData2 );
    if ( userData2 && !bcrypt.compareSync( login.password, userData2.password ) ) { 
      throw new UnauthorizedException("Login or password not correct"); 
    }
    return this.generateTokens( userData, agent );
  }

  private async generateTokens(user: UserEntity, agent: string ): Promise<Tokens> {
    const accessToken =
        // 'Bearer ' +
        this.jwtService.sign({
            id: user.id,
            email: user.email,
            roles: user.roles,
            expiresIn: convertToSecondsUtil( this.configService.get( "EXPIRES_ACCESS" ) ),
            userAgent: agent
        });
    const refreshToken = await this.getRefreshToken( user, agent );
    return { 
      accessToken, 
      refreshToken 
    };
  }

  private async getRefreshToken(user: UserEntity, agent: string ): Promise<string> { 
    return this.generateClientToken(user, agent); 
  }

  private async generateClientToken ( user: UserEntity, userAgent: string ) {
    const token = this.jwtService.sign({ 
      clientId: this.configService.get('CLIENT_ID'),
      userId: user.id,
      email: user.email,
      roles: user.roles,
    });
    const expire = add( new Date(), { days: this.configService.get( "REFRESH_EXPIRES_DAYS" ) } );
    const dto: CreateTokenDto = {
      token,
      expire,
      userId: 1,
      userAgent 
    };
    //const _token = await this.tokenService.create(dto); 
    const _token = await this.tokenService.upsert( dto, user.id, userAgent );
    return token;
  }

  deleteRefreshToken(token: string) { 
    return this.tokenService.remove( token );
  }
}
