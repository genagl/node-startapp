// import { isPublic } from '@common/decorators';
import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport'; 
import { Observable } from 'rxjs';
@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') implements CanActivate {
    constructor(private readonly reflector: Reflector) {
        super();
    }

    canActivate(ctx: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const request = ctx.switchToHttp().getRequest();
        const token = this.extractTokenFromHeader(request);
        if(!token) { 
            throw new UnauthorizedException( );
        }
        return super.canActivate(ctx);
    }

    private extractTokenFromHeader(request: Request): string | undefined { 
        try {
            const [type, token] = request.headers["authorization"]?.split(' ') ?? [];
            return type === 'Bearer' ? token : undefined;
        }
        catch(err) {
            throw new UnauthorizedException( );
        }
    }
}