import { JwtPayload } from '@common/interface';
import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { UsersService } from '@users/users.service';
import { ExtractJwt, Strategy } from 'passport-jwt';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  private readonly logger = new Logger(JwtStrategy.name);
  constructor(
    private readonly configService: ConfigService, 
    private readonly userService: UsersService
  ) { 
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get('SECRET_KEY'),
    });
  }

  async validate(payload: JwtPayload) { 
    //console.log( "JWT-strategy, validate", payload );
    const user = await this.userService.findById( +payload.id ).catch((err) => {
      this.logger.error(err);
      return null;
    }); 
    if (!user || user.isBlocked) {
      throw new UnauthorizedException('У вас нет доступа');
    }

    // проверка валидности рефреш-токена


    return payload;
  }
}
