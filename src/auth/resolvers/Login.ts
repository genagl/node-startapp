import { Field, ObjectType } from "@nestjs/graphql";


@ObjectType({description: "Login data"})
export class Login {
    @Field({ nullable: true, description: 'User access token' })
    accessToken: string;
    
    @Field({ nullable: true, description: 'User refresh token' })
    refreshToken: string;
}