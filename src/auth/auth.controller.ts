import { Cookie, UserAgent } from '@common/decorators';
import { BadRequestException, Body, ClassSerializerInterceptor, Controller, Get, HttpCode, HttpStatus, Post, Res, UnauthorizedException, UseGuards, UseInterceptors } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
//import { HttpService } from '@nestjs/axios';
import { Tokens } from '@common/interface';
import { HttpService } from '@nestjs/axios';
import { UserId } from '@src/users/decorators/user-id.decorator';
import { UsersService } from '@src/users/users.service';
import { UserResponse } from '@users/responses';
import { add } from 'date-fns';
import { Response } from 'express';
import { JwtAuthGuard } from './guards/jwt.guard';
import { UpdateURLAvatarInterceptor } from '@users/interceptors';
import { UserRolesNotNullableInterceptor } from '@users/interceptors/user-roles-not-nullable.interceptor';

export const REFRESH_TOKEN = 'perefreshtoken';
@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
    private readonly userService: UsersService
  ) {}

  
  @HttpCode(HttpStatus.OK)
  @Post('/login') 
  // @ApiConsumes('multipart/form-data')
  @ApiBody({ type: LoginDto })
  @ApiOperation({ summary: "Sign In User" })
  async login( @Body() signInDto: LoginDto, @Res() res: Response, @UserAgent() agent: string  ) { 
    const tokens: Tokens =  await this.authService.signIn( signInDto, agent );
    if (!tokens) {
      throw new BadRequestException(`Не получается войти с данными ${JSON.stringify( signInDto )}`);
    }
    this.setRefreshTokenToCookies(tokens, res);
  }

  
  @Get('logout')
  async logout(@Cookie(REFRESH_TOKEN) refreshToken: string, @Res() res: Response) {
      if (!refreshToken) {
          res.sendStatus(HttpStatus.OK);
          return;
      }
      await this.authService.deleteRefreshToken(refreshToken);
      res.cookie(REFRESH_TOKEN, '', { httpOnly: true, secure: true, expires: new Date() }); 
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Post('/register')
  @ApiConsumes('multipart/form-data')
  @ApiOperation({ summary: "Sign Up User" })
  async register(@Body() dto: CreateUserDto) {
    console.log(dto);
    const user = await this.authService.register(dto);
    if(!user) {
      throw new BadRequestException(`Не получается зарегистрировать пользователя с данными ${JSON.stringify(dto)}`)
    }
    return new UserResponse( user );  
  }

  @Get('refresh-tokens')
  async refreshToken( 
    @Cookie(REFRESH_TOKEN) refreshToken: string, 
    @Res() res: Response, 
    @UserAgent() agent: string
  ) {
    if(!refreshToken) {
      throw new UnauthorizedException();
    }
    const tokens = await this.authService.refreshTokens(refreshToken, agent);
    if(!tokens) {
      throw new UnauthorizedException();
    }
    this.setRefreshTokenToCookies(tokens, res);
  }

  @UseInterceptors(UpdateURLAvatarInterceptor)
  @UseInterceptors(UserRolesNotNullableInterceptor)
  @Get('userInfo')
  @UseGuards( JwtAuthGuard )
  @ApiBearerAuth()
  @ApiOperation({ summary: "Get single Resource by specified id" })
  async userInfo(@UserId() userId: number,  ) {
    const user = await this.userService.findById( userId );
    return new UserResponse( user );
  }

  private setRefreshTokenToCookies(tokens: Tokens, res: Response) {
    if (!tokens) {
      throw new UnauthorizedException();
    }
    res.cookie(REFRESH_TOKEN, tokens.refreshToken, {
        httpOnly: true,
        sameSite: 'lax',
        expires: add( new Date(), { days: this.configService.get("REFRESH_EXPIRES_DAYS") } ),
        secure: this.configService.get('NODE_ENV', 'development') === 'production',
        path: '/',
    });
    res.status(HttpStatus.CREATED).json({ accessToken: tokens.accessToken });
  }
}
