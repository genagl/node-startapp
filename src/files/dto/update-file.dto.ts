import { PartialType } from '@nestjs/swagger';
import { CreateFileDto } from './create-file.dto';
import { InputType } from '@nestjs/graphql';

@InputType()
export class UpdateFileDto extends PartialType(CreateFileDto) {}
