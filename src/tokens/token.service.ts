import { Injectable } from "@nestjs/common";  
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";  
import { TokenEntity } from "./entities/token.entity";
import { CreateTokenDto } from "./dto/create-token.dto";
import { UpdateTokenDto } from "./dto/update-token.dto";

@Injectable()
export class TokenService { 
    constructor( 
        @InjectRepository(TokenEntity)
        private readonly repository: Repository<TokenEntity>,
    ) { } 
    create(dto: CreateTokenDto) { 
        return this.repository.save( dto );
    } 
    update(id: number, updateFileTermDto: UpdateTokenDto) {
        return this.repository.update(id, updateFileTermDto);
    } 
    remove(token: any) {  
        return this.repository.createQueryBuilder()
            .delete()
            .from(TokenEntity)
            .where("token = :token", { token })
            .execute();
    }
    findOne(token: string) {
        return this.repository.findOneBy({ token });
    }
    async upsert( token: CreateTokenDto, userId: number, userAgent: string ) {
        const _token = await this.repository.findOne({ 
            where: {
                userId,
                userAgent, 
            }
        });
        return this.repository.upsert( 
            [
                {id: _token?.id, ...token },
            ],
            ["id"] 
        );
    }
}