import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TokenEntity } from './entities/token.entity';
import { TokenController } from './token.controller';
import { TokenResolver } from './token.resolver';
import { TokenService } from './token.service';


@Module({ 
    imports: [ TypeOrmModule.forFeature([ TokenEntity ])], 
    providers: [ TokenService, TokenResolver ], 
    controllers: [ TokenController, ] ,
    exports: [ TokenService ]
})
export class TokensModule {

}
