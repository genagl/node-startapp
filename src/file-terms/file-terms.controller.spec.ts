import { Test, TestingModule } from '@nestjs/testing';
import { FileTermsController } from './file-terms.controller';
import { FileTermsService } from './file-terms.service';

describe('FileTermsController', () => {
  let controller: FileTermsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FileTermsController],
      providers: [FileTermsService],
    }).compile();

    controller = module.get<FileTermsController>(FileTermsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
