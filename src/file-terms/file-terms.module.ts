import { Module } from '@nestjs/common';
import { FileTermsService } from './file-terms.service';
import { FileTermsController } from './file-terms.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FileTermEntity } from './entities/file-term.entity';
import { FileTermResolver } from './file-terms.resolvers';

@Module({
  controllers: [FileTermsController],
  providers: [FileTermsService, FileTermResolver],
  imports: [TypeOrmModule.forFeature([FileTermEntity])], 
})
export class FileTermsModule {}
