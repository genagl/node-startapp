import { Args, Int, Query, Mutation, Resolver } from '@nestjs/graphql'; 
import { FileTermEntity } from './entities/file-term.entity'; 
import { FileTermsService } from './file-terms.service';
import { UpdateFileTermDto } from './dto/update-file-term.dto';

@Resolver( of => FileTermEntity )
export class FileTermResolver { 
  constructor(private readonly fileTermService: FileTermsService ) {}

  @Query( () => FileTermEntity, { name: 'getFileTerm',  nullable: true } )
  async getFileTerm(@Args('id', { type: () => Int }) id: number): Promise<FileTermEntity> {
    const file = await ( this.fileTermService.findOne( id ) ) 
    return file
  }
  
  @Query(() => [FileTermEntity], {  name: 'getFileTerms',  nullable: 'items' })
  async getFileTerms( ): Promise<FileTermEntity[]> {
    const files = await ( this.fileTermService.findAll( ) ) 
    return files
  }
  @Mutation( () => FileTermEntity, {  name: 'removeFileTerm'} )
  remove( ids: string ) { 
    return this.fileTermService.remove( ids);
  }

  @Mutation( () => FileTermEntity, {  name: 'updateFileTerm'} )
  update( id: number, updateFileTermDto: UpdateFileTermDto) {
    return this.fileTermService.update(id, updateFileTermDto);
  }
}
