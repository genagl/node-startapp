import { Injectable } from '@nestjs/common';
import { CreateFileTermDto } from './dto/create-file-term.dto';
import { UpdateFileTermDto } from './dto/update-file-term.dto';
import { FileTermEntity } from './entities/file-term.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class FileTermsService {
  constructor(
    @InjectRepository(FileTermEntity)
    private repository: Repository<FileTermEntity>,
  ) {}

  create(createFileTermDto: CreateFileTermDto) {
    return this.repository.save( createFileTermDto );
  }

  findAll() {
    const qb = this.repository.createQueryBuilder('fileTerm');
    return qb.getMany();
  } 

  update(id: number, updateFileTermDto: UpdateFileTermDto) {
    return this.repository.update(id, updateFileTermDto);
  }

  remove(ids: string) {
    const idsArray = ids.split(',');

    const qb = this.repository.createQueryBuilder('fileTerm');

    qb.where('id IN (:...ids)', {
      ids: idsArray, 
    });

    return qb.softDelete().execute();
  }
  
  findOne(id: number) {
    return this.repository.findOneBy({ id });
  }

}
