import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { FileTermsService } from './file-terms.service';
import { CreateFileTermDto } from './dto/create-file-term.dto';
import { UpdateFileTermDto } from './dto/update-file-term.dto';
import { Resolver } from '@nestjs/graphql';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';

@Resolver( of => FileTermsService )
@ApiTags('file-terms')
@Controller('file-terms')
export class FileTermsController {
  constructor(private readonly fileTermsService: FileTermsService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth() 
  create(@Body() createFileTermDto: CreateFileTermDto) {
    return this.fileTermsService.create(createFileTermDto);
  }

  @Get()
  findAll() {
    return this.fileTermsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.fileTermsService.findOne(+id);
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth() 
  update(@Param('id') id: string, @Body() updateFileTermDto: UpdateFileTermDto) {
    return this.fileTermsService.update(+id, updateFileTermDto);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth() 
  remove(@Param('id') id: string) {
    return this.fileTermsService.remove( id );
  }
}
