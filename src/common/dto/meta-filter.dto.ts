import { I_META_FILTER } from "@common/interface";
import { Field, InputType, Int } from "@nestjs/graphql";
import { ApiProperty } from "@nestjs/swagger";

@InputType("MetaFilter")
export class MetaFilter implements I_META_FILTER{

    @ApiProperty( ) 
    @Field(() => String, { description: "key of meta-field for filter", nullable: true  }) 
    key?: string | null;
    
    @ApiProperty( ) 
    @Field(() => String, { description: "value of meta-field for filter", nullable: true  }) 
    value?: string | null;
    
    @ApiProperty( ) 
    @Field(() => Int, { description: "value numeric of meta-field for filter", nullable: true  }) 
    value_numeric?: number | null;
    
    @ApiProperty( ) 
    @Field(() => Boolean, { description: "value boolean of meta-field for filter", nullable: true  }) 
    value_bool?: boolean | null;
    
    @ApiProperty( ) 
    @Field(() => String, { description: "compare (=, !=, >, <, >=, <=, EXISTS, NOT EXISTS, IN, NOT IN, BETWEEN, NOT BETWEEN)", nullable: true  }) 
    compare?: "=" | "!=" | ">" | "<" | ">=" | "<=" | "EXISTS" | "NOT EXISTS" | "IN" | "NOT IN" | "BETWEEN" | "NOT BETWEEN";


    @ApiProperty( ) 
    @Field(() => String, { description: "target value for compare", nullable: true  }) 
    target?: number | string | number[] | string[] | null;

    //key(string)
    //value(string)
    //value_numeric(int)
    //value_bool(boolean)
    //compare(string) (=, !=, >, <, >=, <=, EXISTS, NOT EXISTS, IN, NOT IN, BETWEEN, NOT BETWEEN)
}