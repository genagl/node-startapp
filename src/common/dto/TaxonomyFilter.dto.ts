import { TAXONOMY_FILTER } from "@common/interface";
import { Field, ID, InputType} from "@nestjs/graphql";
import { ApiProperty } from "@nestjs/swagger";


@InputType()
export class TaxonomyFilter implements TAXONOMY_FILTER {
    @ApiProperty( ) 
    @Field(() => String, { description: "key of meta-field for filter", nullable: true  }) 
    tax_name: string | null;

    @ApiProperty( ) 
    @Field(() => [ID], { description: "key of meta-field for filter", nullable: true  }) 
    term_ids: number[];
}