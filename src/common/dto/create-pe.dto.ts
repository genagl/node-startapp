import { Field, ID, InputType, Int } from "@nestjs/graphql";
import { ApiProperty } from "@nestjs/swagger";
import { IsOptional } from "class-validator";
import { PEEntity } from "../entities/pe.entity";
import { POST_STATUS } from "../interface/postStatuses";


@InputType()
export abstract class CreatePEDto extends PEEntity { 

    @ApiProperty({default: "title"})
    @Field(() => String, { description: "Title of pe-resource", nullable: true  }) 
    title?: string | null;

    @ApiProperty({default: ""}) 
    @Field(() => String, { 
        deprecationReason: "Outdated. Used for compatibility with the API wp-server. Use the 'content' field",
        description: "Content of pe-resource", 
        nullable: true  
    }) 
    post_content?: string | null;

    @ApiProperty({default: ""}) 
    @Field(() => String, { description: "Content of pe-resource", nullable: true  }) 
    content?: string | null;

    @ApiProperty() 
    @Field(() => String, { description: "Thumbnail of pe-resource", nullable: true  }) 
    thumbnail?: string | null;

    @ApiProperty({default: 1})
    @Field(() => Int, { description: "Order", nullable: true }) 
    @IsOptional()
    order?: number | null;

    @ApiProperty({default: 1})
    @Field(() => Int, { description: "Author ID", nullable: true }) 
    @IsOptional()
    author_id?: number | null;

    @ApiProperty({ default: POST_STATUS.PUBLISH })
    @Field(() => POST_STATUS, {defaultValue: POST_STATUS.PUBLISH, nullable: true, }) 
    @IsOptional()
    post_status: POST_STATUS;

    @ApiProperty({ name: "thumb image", type: 'string', format: 'binary', required: true }) 
    @IsOptional()
    file?: Express.Multer.File
  
    @ApiProperty({ name: "Land ID"})
    @Field( () => ID, { nullable: true } ) 
    land_id?: number | string;
  
    @ApiProperty()
    @Field( { nullable: true } ) 
    post_date?: Date
}