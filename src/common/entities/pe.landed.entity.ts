import { Field, ID, Int, ObjectType } from "@nestjs/graphql";
import { PEEntity } from "./pe.entity";
import { LandEntity } from "@src/lands/entities";
import { Column } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";


@ObjectType({ isAbstract: true })
export class PELandedEntity extends PEEntity { 
    
    @Column("integer",{ nullable: true, default: 0 }) 
    @Field(() => ID, { nullable: true } )
    @ApiProperty({ name: "Land ID"})
    land_id?: number | string;

    @Field(() => LandEntity, { description: 'Land position of Resource' }) 
    land?: LandEntity;
}
