import { Field, ID, Int, ObjectType } from '@nestjs/graphql';
import { ApiHideProperty, ApiProperty } from "@nestjs/swagger";
import { MaxLength, MinLength } from 'class-validator';
import { POST_STATUS } from "src/common/interface/postStatuses";
import { UserEntity } from "src/users/entities/user.entity";
import { Column, PrimaryGeneratedColumn } from "typeorm";
import { IPe } from "../interface/pe-interface";

@ObjectType({ isAbstract: true })
export abstract class PEEntity implements IPe { 

    @PrimaryGeneratedColumn()
    @Field(type => Int, { nullable: true,  description: "Uniq identifier." })
    id: number;

    @Field(() => Int, { 
        nullable: true,  
        deprecationReason: "Use 'id' field! This field added for backward compatibility with legacy APIs. Will be removed in the next version.",
        description: "Uniq identifier." 
    })
    ID: number;

    @Column("varchar", {length: 150, nullable: true, default: "title" }) 
    @MaxLength(150)
    @Field(() => String, { description: "Title of pe-resource", nullable: true }) 
    @ApiProperty({ nullable: true  })
    title?: string;

    @Column("varchar", {nullable: true, default: "" }) 
    @Field(() => String, { description: "Icon", nullable: true }) 
    @ApiProperty({ nullable: true  })
    icon?: string;

    @ApiProperty({ nullable: true  })
    @Field(() => String, { description: "Logotype", nullable: true }) 
    logotype?: string;
 
    @Field(() => String, { 
        defaultValue: "",
        description: "HTML content of pe-resource", 
        deprecationReason: "Outdated. Used for compatibility with the API wp-server. Use the 'content' field",
        nullable: true 
    }) 
    @ApiProperty({ example: " ", nullable: true  })
    post_content?: string;

    @Column("varchar", { nullable: true, default: "" })  
    @Field(() => String, { defaultValue: "", description: "HTML content of pe-resource", nullable: true }) 
    @ApiProperty({ example: " ", nullable: true  })
    content?: string;

    @Column("varchar", {length: 50, nullable: true }) 
    @MinLength(3)
    @MaxLength(50)
    @Field(() => String, {
        nullable: true, 
        defaultValue: "", 
        description: "Resource optional password. Max length — 50." 
    }) 
    @ApiProperty({ 
        example: "", 
        required: false, 
        nullable: true, 
        description: "Resource optional password. Max length — 50." 
     })
    password: string;

    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'Order' })
    @Field(() => Int, { nullable: true } )
    order?: number;

    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'Author ID' })
    @Field(() => Int, { nullable: true } )
    author_id?: number;
    
    @Field(() => UserEntity, { description: 'Resource author' }) 
    author?: UserEntity;  
    
    @Field(() => UserEntity, { 
        description: 'Resource author',
        deprecationReason: "Use 'author' field! This field added for backward compatibility with legacy APIs. Will be removed in the next version."
    }) 
    post_author?: UserEntity;  
    
    @Column( { type: "enum", enum: POST_STATUS, default: POST_STATUS.PUBLISH })
    @Field(() => POST_STATUS, { 
        nullable: true, 
        description: 'Resource visible status', 
        defaultValue: POST_STATUS.PUBLISH 
    })
    @ApiProperty({ 
        enum: POST_STATUS, 
        enumName: "POST_STATUS", 
        example: POST_STATUS.PUBLISH, 
        required: false, 
        description: 'Resource visible status' 
    })
    post_status: POST_STATUS;

    @Column({ nullable: false, default: false })
    @Field({  description: 'Is resource blocked by admin?', defaultValue: false })
    @ApiProperty({example: false, required: false, description: 'Is resource blocked by admin?' })
    is_blocked?: boolean;

    @Column({ nullable: true }) 
    @Field(() => String, {  nullable: true, defaultValue: "", description: "URL of thumbnail" }) 
    @ApiProperty({ example: "", required: false, description: 'URL of thumbnail' })
    thumbnail?: string;

    @Column({ nullable: true }) 
    @Field(() => String, {  nullable: true, defaultValue: "", description: "File name of media." }) 
    @ApiHideProperty()
    thumbnail_name?: string;

    @Column({ nullable: true }) 
    @Field(() => ID, {  nullable: true, defaultValue: "", description: "Uniq ID of media file" }) 
    @ApiHideProperty()
    thumbnail_id?: number; 
    
    @Column({ nullable: true }) 
    @Field({  nullable: true, description: "Date" })  
    @ApiProperty({ name: "Date"}) 
    post_date?: Date | null;
}
