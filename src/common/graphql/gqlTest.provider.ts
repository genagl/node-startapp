import { Mutation, Query, Resolver } from "@nestjs/graphql"; 


@Resolver()
export class GqlTestProvider {
    @Query(() => Boolean, {name: "getTest", description: "Test query"})
    getTest() : Promise<boolean> {
        return new Promise(resolver => {
            resolver(true);
        });
    }
    @Mutation(() => Boolean, {name: "setTest", description: "Test mutation"})
    setTest() : Promise<boolean> {
        return new Promise(resolver => {
            resolver(true);
        });
    }
}