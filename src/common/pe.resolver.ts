import { GraphqlAuthGuard } from "@auth/guards/jwt.gql.guard";
import { GraphqlRolesGuard } from "@auth/guards/roles.gql.guard";
import { SetMetadata, UseGuards, UseInterceptors } from "@nestjs/common";
import { Args, ID, Int, Mutation, Parent, Query, ResolveField, Resolver } from "@nestjs/graphql";
import { LandEntity } from "@lands/entities";
import { UserEntity } from "@users/entities/user.entity";
import { Role } from "@users/models/roles.enum";
import { DeepPartial } from "typeorm";
import { IPEResolver, IPESingleMember, IPe } from "./interface";
import { IPERepository } from "./repositories";
import { plural } from "./utils";  
import PagingDto from "@common/dto/paging.dto"; 
import { UpdateURLIconInterceptor } from "@lands/interceptors";

export function PEResolver<T extends IPe | unknown, K extends DeepPartial<T>>(classRef: T, InputRef: K, className: string): any {
    @Resolver(() => classRef, { isAbstract: true })
    abstract class PEBaseResolver implements IPEResolver<T, K> {
        private _repository: IPERepository<T, K>;  
        private _landRepository: IPESingleMember<LandEntity>; 
        private _userRepository: IPESingleMember<UserEntity>; 
        constructor( 
            _repository: IPERepository<T, K>,   
            _landRepository: IPESingleMember<LandEntity>, 
            _userRepository: IPESingleMember<UserEntity>, 
        ) {  
            this._repository = _repository; 
            this._landRepository = _landRepository;  
            this._userRepository = _userRepository;  
        }

        @UseInterceptors(UpdateURLIconInterceptor)
        @Query( () => classRef, { name: `get${ className }`, description: `Single ${className}` } )
        async getPE(
            @Args('id', { type: () => String, nullable: true }) id: number | string,
            @Args('land_id', { type: () => ID, nullable: true }) land_id?: number | string,
        ): Promise<T> | null {
            const file = await ( this._repository.findById( id, parseInt(land_id?.toString()) ) );
            return file;
        }

        @UseInterceptors(UpdateURLIconInterceptor)
        @Query( () => [classRef], { name: `get${plural(className)}`, nullable: "items", description:  `List of ${className}`} )
        async getAllPE( 
            @Args("paging", {type: () => PagingDto, nullable: true }) paging: PagingDto,
            @Args('land_id', { type: () => ID, nullable: true }) land_id?: number | string,
        ): Promise<T[]> { 
            const file = await ( this._repository.findFilter( paging, parseInt(land_id?.toString()) ) );
            return file;
        }

        @Query( () => Int, { name: `get${ className }Count`, nullable: true, description: `Full count of ${className}s list` } )
        async getAllPECount( 
            @Args("paging", {type: () => PagingDto, nullable: true }) paging: PagingDto,
            @Args('land_id', { type: () => ID, nullable: true }) land_id?: number | string,
        ): Promise<number> { 
            paging = {...paging, count: 0, offset: 0 };
            const file = await ( this.getAllPE(paging, land_id));
            return file.length;
        }
        
        @Mutation(() => classRef, { name: `create${className}`, description: `Create single ${className}` })
        async create( 
            @Args('input', { name: `${className}Input`, type: () => InputRef }) updateInput: K,
            @Args('land_id', { type: () => ID, nullable: true }) land_id?: number | string,
        ) {
            return await this._repository.create( updateInput, parseInt(land_id?.toString()) );
        }
                
        @Mutation(() => classRef, { name: `change${className}`, nullable: true, description: `Update single ${className}` })
        async update(
            @Args('id', { type: () => String, nullable: true }) id: number | string,
            @Args('input', { name: `${className}Input`, type: () => InputRef, nullable: true }) updateInput: K,
            @Args('land_id', { type: () => ID, nullable: true }) land_id?: number | string,
        ) {
            return await this._repository.update( id, updateInput, parseInt(land_id?.toString()) );
        }
    
        @Mutation(() => Int, { name: `delete${className}`, nullable: true, description: `Remove single ${className}` })
        @UseGuards(GraphqlAuthGuard, GraphqlRolesGuard)
        @SetMetadata('roles', [ Role.Administrator, Role.Author ])
        async remove(
            @Args('id', { type: () => Int }) id: number, 
            @Args('land_id', { type: () => ID, nullable: true }) land_id?: number | string,
        ): Promise<number> {
            await this._repository.remove(id, parseInt(land_id?.toString()));
            return id;
        }
        
        @ResolveField("land", () => LandEntity, { complexity: 4, nullable: true, description: "Owner Land of Resource." })
        async land(@Parent() author:  IPe ) {
            const land_id: number | string = author['land_id'];
            return land_id 
                ? 
                this._landRepository.findById( parseInt(land_id?.toString()) )
                : 
                null;
        }

        @ResolveField("author", () => UserEntity, { complexity: 4, nullable: true, description: "User, that create Land." })
        @ResolveField("post_author", () => UserEntity, { complexity: 4, nullable: true, description: "User, that create Land." })
        async author(@Parent() author:  T ) {
            const author_id: number = author['author_id'];  
            return author_id 
                ? 
                this._userRepository.findById( author_id )
                : 
                null; 
        }
    } 
    return PEBaseResolver;
}