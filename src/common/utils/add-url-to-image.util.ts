import { Request } from "express"

export const setURLImage = (req: Request, avatar_name: string): string => {
    return avatar_name.indexOf("http") > -1 
    ?
    avatar_name
    :
    `${req.protocol}://${req.get('Host')}/avatars/${avatar_name}`;
}