import { ConfigService } from "@nestjs/config";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { AppModule } from "src/app.module";
import { description, name, version } from "../../../package.json";
import { INestApplication } from "@nestjs/common";

export const getSwagger = ( app: INestApplication, ) => {
    const configService = app.select(AppModule).get( ConfigService ); 
    const config = new DocumentBuilder()
        .setTitle(`${ name }`)
        .setDescription(`${ description }`)
            .setVersion(version)
            .addBearerAuth().addBasicAuth()
                .addTag('auth')
                .build();
    const document = SwaggerModule.createDocument(
        app, 
        config,
        {
        extraModels: [],
        }
    );
    SwaggerModule.setup(
        configService.get( "SWAGGER_API_ENDPOINT" ), 
        app, 
        document, 
        {
        swaggerOptions: {
            persistAuthorization: true,
            swaggerUiEnabled: false,
        },
        }
    );
}