import { CustomScalar, Scalar } from "@nestjs/graphql";
import { Kind, ValueNode } from "graphql";

@Scalar('URL', () => URLScalar)
export class URLScalar implements CustomScalar<string, string> {
  description = 'Hash scalar';

  parseValue(value: string): string { 
    return value.toString();
  }

  serialize(value: string): string { 
    return (value).toString();
  }

  parseLiteral(ast: ValueNode): string { 
    if (ast.kind === Kind.STRING) {
      return this.parseValue(ast.value);
    }
    return null;
  }
}