import { CacheModule } from "@nestjs/cache-manager";
import { Module } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { redisStore } from 'cache-manager-redis-yet';

@Module({
    imports: [
        CacheModule.registerAsync({  
            isGlobal: true,  
            useFactory: async (configService: ConfigService) => ({  
                store: await redisStore({  
                    socket: {  
                        host: configService.get('REDIS_HOST'),  
                        port: configService.get("REDIS_PORT"),  
                        passphrase: configService.get('REDIS_PASSWORD'),
                    },        
                }),      
            }),    
        }), 
    ]
})
export class CacheRedisModule{}