import { CreateOptionInput } from './create-option.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType("OptionsInput")
export class UpdateOptionInput extends PartialType(CreateOptionInput) {
    
}
