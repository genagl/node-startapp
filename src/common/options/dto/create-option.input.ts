import { Field, InputType } from '@nestjs/graphql'; 
import { IsEmail } from 'class-validator';

@InputType("OptionInput")
export class CreateOptionInput { 
  @Field(() => String, {defaultValue: "New site", description: "Name of site" }) 
  name: string;
   
  @Field(() => String, {defaultValue: "", description: "Description of site" }) 
  description: string;
  
  @Field(() => String, {defaultValue: "", description: "Administrator email", nullable: true }) 
  @IsEmail()
  email: string;
  
  @Field(() => String, {defaultValue: "", description: "Public address", nullable: true }) 
  address: string;
  
  @Field(() => String, { description: "Help site URL", nullable: true }) 
  help_url: string;
  
  @Field(() => String, { description: "VK app ID", nullable: true }) 
  vk_app_id: string;
   
  @Field(() => String, { description: "VK link", nullable: true }) 
  vk: string;
  
  @Field(() => String, { description: "youtube link", nullable: true }) 
  youtube: string;
  
  @Field(() => String, { description: "Default image name", nullable: true }) 
  default_img_name: string;
   
  @Field(() => String, { description: "Default image ID", nullable: true }) 
  default_img_id: string;
  
  @Field(() => String, { description: "Default image", nullable: true }) 
  default_img: string;
  
  @Field(() => String, { description: "Thumbnail name", nullable: true }) 
  thumbnail_img_name: string;
  
  @Field(() => String, { description: "Thumbnail ID", nullable: true }) 
  thumbnail_id: string;
  
  @Field(() => String, { description: "Thumbnail image", nullable: true }) 
  thumbnail_img: string;
  
  @Field(() => Boolean, { description: "New User must e-mail activate account" })
  user_verify_account: boolean;
}
