import { Field, ID, ObjectType } from "@nestjs/graphql";
import { ApiProperty } from "@nestjs/swagger"; 
import { IsEmail, MaxLength } from "class-validator";
import { Column, Entity, ObjectLiteral, PrimaryGeneratedColumn } from "typeorm"; 

@Entity("options")
@ObjectType( "Options" ) 
export class OptionEntity implements ObjectLiteral { 

    @PrimaryGeneratedColumn()
    @Field(() => ID, { nullable: true,  description: "Uniq identifier." })
    id: number;
    
    @Column("varchar", {length: 150, nullable: true, default: "New site" }) 
    @MaxLength(150)
    @Field(() => String, {defaultValue: "New site", description: "Name of site" }) 
    @ApiProperty({ example: "New site", nullable: true  })
    name: string
    
    @Column("varchar", {length: 450, nullable: true, default: "" }) 
    @MaxLength(450)
    @Field(() => String, {defaultValue: "", description: "Description of site" }) 
    @ApiProperty({ example: "", nullable: true  })
    description: string
    
    @Column("varchar", { nullable: true, default: "" })  
    @Field(() => String, {defaultValue: "", description: "Administrator email" }) 
    @ApiProperty({ example: "", nullable: true  })
    @IsEmail()
    email?: string | null;
    
    @Column("varchar", { nullable: true, default: "" })  
    @Field(() => String, {defaultValue: "", description: "Public address" }) 
    @ApiProperty({ example: "", nullable: true  })
    address?: string | null;
    
    @Column("varchar", { nullable: true, default: "" })  
    @Field(() => String, { description: "Help site URL" }) 
    @ApiProperty({ example: "", nullable: true  })
    help_url?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "VK app ID" }) 
    @ApiProperty({ nullable: true  })
    vk_app_id?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "VK link" }) 
    @ApiProperty({ nullable: true  })
    vk?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "youtube link" }) 
    @ApiProperty({ nullable: true  })
    youtube?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Default image name" }) 
    @ApiProperty({ nullable: true  })
    default_img_name?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Default image ID" }) 
    @ApiProperty({ nullable: true  })
    default_img_id?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Default image" }) 
    @ApiProperty({ nullable: true  })
    default_img?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Thumbnail name" }) 
    @ApiProperty({ nullable: true  })
    thumbnail_img_name?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Thumbnail ID" }) 
    @ApiProperty({ nullable: true  })
    thumbnail_id?: string | null;
    
    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Thumbnail image" }) 
    @ApiProperty({ nullable: true  })
    thumbnail_img?: string | null;
    
    @Column("boolean", { nullable: true })  
    @Field(() => Boolean, { description: "New User must e-mail activate account" }) 
    @ApiProperty({ nullable: true  })
    user_verify_account?: boolean | false;

}