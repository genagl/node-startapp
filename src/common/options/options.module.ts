import { Module } from '@nestjs/common';
import { OptionsService } from './options.service';
import { OptionsResolver } from './options.resolver';
import { OptionsController } from './options.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OptionEntity } from './entities/option.entity';

@Module({
  providers: [ OptionsResolver, OptionsService ], 
  controllers: [ OptionsController ],
  imports: [ TypeOrmModule.forFeature([ OptionEntity, ]), ]
})
export class OptionsModule {}
