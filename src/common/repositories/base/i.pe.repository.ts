import { DeepPartial, FindManyOptions, FindOneOptions } from 'typeorm';

export interface IPERepository<T, K> {
    create(data: DeepPartial<T>, land_id?:number): Promise<T>; 
    //save(data: DeepPartial<T>): Promise<T>;
    //saveMany(data: DeepPartial<T>[]): Promise<T[]>;
    findById(id: number | string, land_id?:number): Promise<T>;
    findByIds(id: number[], land_id?:number): Promise<T[]>;
    //findByCondition(filterCondition: FindOneOptions<T>): Promise<T>;
    //findWithRelations(relations: FindManyOptions<T>): Promise<T[]>; 
    findAll(options?: FindManyOptions<T>, land_id?:number): Promise<T[]>;
    findFilter(options?: any, land_id?:number): Promise<T[]>;
    remove(ids: string | number, land_id?:number): Promise<T>;
    update(id: number | string, dto: any, land_id?:number):  Promise<T>;
}   