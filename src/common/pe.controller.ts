import { Body, Delete, Get, HttpStatus, MaxFileSizeValidator, Param, ParseFilePipe, ParseIntPipe, Patch, Post, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiOperation, ApiParam, ApiResponse } from '@nestjs/swagger';
import { CreatePEDto } from './dto/create-pe.dto';
import { PEEntity } from './entities/pe.entity';  
import { FileInterceptor } from '@nestjs/platform-express';
import { fileStorage } from 'src/files/storage';
import { FilesService } from 'src/files/files.service';
import { JwtAuthGuard } from '@auth/guards/jwt.guard';
 export abstract class PEController {
  private _service: any;
  private readonly _fileService: FilesService;

  constructor( _service: any ) {
    this._service = _service;
  }
  
  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth() 
  @UseInterceptors( FileInterceptor('file', { storage: fileStorage, }), )
  @ApiConsumes('multipart/form-data')
  @ApiOperation({ summary: "Create a Resource with specified id" })
  create(
    @UploadedFile(
      new ParseFilePipe({
        validators: [new MaxFileSizeValidator({ maxSize: 1024 * 1024 * 5 })],
      }),
    )
    file: Express.Multer.File,
    @Body() createUserDto: CreatePEDto
  ) {
    // create thumbnail
    /*
    if(file) {
      const _file = this._fileService.create(
        file, 1, 1
      );
      console.log( _file );
      createUserDto = {
        ...createUserDto,
        thumbnail: file.filename,
        thumbnail_id: 111,
        thumbnail_name: file.filename
      }
    }
    */
    return this._service.create( createUserDto );
  }

  @Get()
  @ApiOperation({ summary: "Get Resource list" })
  findAll() {
    return this._service.findAll();
  }

  @Get(':id')
  @ApiOperation({ summary: "Get single Resource by specified id" })
  findOne(@Param('id') id: number) {
    return this._service.findById( id );
  }

  @Patch(':id') 
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth() 
  @ApiOperation({ summary: "Updates a Resource with specified id" })
  @ApiParam({ name: "id", required: true, description: "Resource identifier" })
  @ApiResponse({ status: HttpStatus.OK, description: "Success", type: PEEntity })
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, description: "Bad Request" })
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: "Unauthorized" })
  update(@Param('id', new ParseIntPipe()) id: string, @Body() dto: CreatePEDto) {
    return this._service.update( id, dto );
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth() 
  @ApiOperation({ summary: "Remove Resource by ID" })
  remove(@Param('id') id: string) {
    return this._service.remove( id );
  }

}
