import { Controller, Delete, Get, HttpStatus, Patch, Post, UseGuards } from '@nestjs/common';
import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { CommentService } from './comment.service';
import { PECommentEntity } from './entities/pe.comment.entity';
import { CreateCommentInput } from './dto/create-comment.input';
import { CommentInput } from './dto/update-comment.input';
import { JwtAuthGuard } from '@src/auth/guards/jwt.guard';
import { ApiBearerAuth, ApiConsumes, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import {ID as _ID} from "../interface/scalars"

@Resolver( () => CommentService )
@ApiTags('comment') 
@Controller('comment')
export class CommentController {
    constructor(private readonly commentService: CommentService) {}
    
    @Post() 
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth() 
    @ApiConsumes('multipart/form-data')
    @Mutation(() => PECommentEntity, { name: `createComment` })
    @ApiOperation({ summary: "Create a Comment." })
    createComment(@Args('CommentInput') createCommentInput: CreateCommentInput) {
        return this.commentService.create(createCommentInput);
    }

    @Get()
    @ApiOperation({ summary: "Get Comment list." }) 
    findAll() {
        return this.commentService.findAll();
    }

    @Get(':id') 
    @ApiOperation({ summary: "Get single Comment by specified id" })
    findOne(@Args('id', { type: () => Int }) id: number) {
        return this.commentService.findOne(id);
    }

    @Patch(':id') 
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth() 
    @ApiOperation({ summary: "Updates a Comment with specified id" })
    @ApiParam({ name: "id", required: true, description: "Comment identifier" })
    @ApiConsumes('multipart/form-data')
    @ApiResponse({ status: HttpStatus.OK, description: "Success", type: PECommentEntity })
    @ApiResponse({ status: HttpStatus.BAD_REQUEST, description: "Bad Request" })
    @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: "Unauthorized" })
    updateComment(
        @Args('id', { type: () => Int }) id: number, 
        @Args('CommentInput') updateCommentInput: CommentInput
    ) {
        return this.commentService.update( id, updateCommentInput) ;
    }

    @Delete(':id') 
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth() 
    @ApiOperation({ summary: "Remove Comment by ID" })
    removeComment(@Args('id', { type: () => Int }) id: number) {
        return this.commentService.remove(id);
    }    
}
