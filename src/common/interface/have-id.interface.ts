import { ID } from "@common/interface";

export interface HaveId {
    id: ID
}