import { HaveId } from "@common/interface";

export interface IPESingleMember<T extends HaveId> {
    findById(id: number) : Promise<T>;
}