import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

export const UserAgentGql = createParamDecorator((_: string, context: ExecutionContext) => {
    const ctx = GqlExecutionContext.create(context);
    const req = ctx.getContext().req;
    // const request = ctx.switchToHttp().getRequest();
    return req.headers['user-agent'];
});
