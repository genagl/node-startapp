import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

export const CookieGql = createParamDecorator((key: string, context: ExecutionContext) => {
    const ctx = GqlExecutionContext.create(context);
    const req = ctx.getContext().req;
    //const request = ctx.switchToHttp().getRequest();
    return key && key in req?.cookies ? req.cookies[key] : key ? null : req.cookies;
});
